title: Example 2
---

# Example 2

In physics, the mass-energy equivalence is stated 
by the equation $E=mc^2$, discovered in 1905 by Albert Einstein.

The mass-energy equivalence is described by the famous equation

\[E=mc^2\]

discovered in 1905 by Albert Einstein. 
In natural units $c$ = 1, the formula expresses the identity

\begin{equation}
E=m
\end{equation}