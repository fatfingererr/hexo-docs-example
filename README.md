# Hexo-theme-docs 範例（含 MathJax ）

本 codebase 是一個 Hexo-theme-docs 的範例，此外可以用 MathJax 

如果你不想要連本 codebase 的 node_module 一起抓，請到 hexo-theme-docs 抓範例

```
$ git clone https://github.com/zalando-incubator/hexo-theme-doc-seed.git
```

並且安裝套件：

```
cd hexo-theme-doc-seed && npm install hexo-cli -g && npm install
```

然後執行此行修改 layout 來 include mathjax :

```
cp -f layout.new.ejs node_modules/hexo-theme-doc/layout/layout.ejs
```

Windows 系統請用 :

```
copy layout.new.ejs node_modules\hexo-theme-doc\layout\layout.ejs /Y
```